import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoList } from './auto/auto-list/auto-list.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';


const appRoute: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'auto', component: AutoList },
  { path: '', redirectTo:'/home', pathMatch:'full' },
  { path: '**', component: HomeComponent}
]

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoute,
      { enableTracing: false } 
      )
  ],

  exports: [
      RouterModule
  ]
})

export class AppRoutingModule { }
