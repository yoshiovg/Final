import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Auto } from '../auto.model';
import { Route } from '@angular/compiler/src/core';
import { AutoService } from '../auto.service';

@Component({
  selector: 'avg-auto-list',
  templateUrl: './auto-list.component.html',
  styleUrls: ['./auto-list.component.css']
})
export class AutoList {
  autos:Auto[];

  constructor(private router: Router, private autoService: AutoService)
   {
     this.autoService.getAutos()
     .subscribe(autos => 
      {
      console.log(autos);
      this.autos = autos;
      }, error => 
        {
        console.log(error);
        });
    console.log("async");
  }

goToAutoDetail(autos: Auto) 
  {
        console.log(autos);
        this.router.navigate(['/auto', autos.id]);
  }
}
