import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { AutoRoutingModule } from './auto-routing.module';
import { AutoList } from './auto-list/auto-list.component';
import { AutoService } from './auto.service';

@NgModule({
  imports: [
      CommonModule,
      AutoRoutingModule
  ],
  declarations: [
     AutoList
  ],
  providers: [AutoService]
})
export class AutoModule { }