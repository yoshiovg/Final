export class Auto {
    id: number;
    anio: number;
    color: string;
    marca: string;
    modelo: string;

    constructor(id: number, anio: number, color: string, marca: string, modelo: string) {
        this.id = id;
        this.anio = anio;
        this.color = color;
        this.marca = marca;
        this.modelo = modelo;
    }
}