import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AutoList } from './auto-list/auto-list.component';

const autoRoute: Routes = [
    { path: 'auto', component: AutoList }
]

@NgModule({
  imports: [
    RouterModule.forChild(
        autoRoute
      )
  ],

  exports: [
      RouterModule
  ]
})

export class AutoRoutingModule { }
