import { Injectable } from '@angular/core';
import { Auto } from './auto.model';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AutoService {

  constructor(public http: HttpClient) { }

  autos: Array<Auto> = [];

  saludar() {
    return "Hola soy un servicio";
  }

  getAutos(): Observable<Array<Auto>> {
    return this.http.get<Array<Auto>>('http://localhost:8080/auto')
      .pipe(
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.log(error.error.message);
    } else {
      console.log(error.status, error.error);
    }
    return new ErrorObservable('ERROR');
  }
}
