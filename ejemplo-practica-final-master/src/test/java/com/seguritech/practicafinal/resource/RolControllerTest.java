package com.seguritech.practicafinal.resource;

import com.seguritech.practicafinal.domain.Rol;
import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static com.seguritech.practicafinal.resource.RolResource.URI;
import com.seguritech.practicafinal.service.RolService;

/**
 *
 * @author Werner
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RolControllerTest {

    private MockMvc mockMvc;

    private CrudResource rolController;

    @Mock
    private RolService rolService;

    public RolControllerTest() {
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Rol rol = new Rol();
        rol.setId(1L);
        rol.setDescripcion("ADMINISTRADOR");
        Mockito.when(rolService.findOne(1L)).thenReturn(rol);

        rolController = new RolResource(rolService);

        mockMvc = MockMvcBuilders.standaloneSetup(rolController).build();
    }

    @Test
    public void testGetRol_isOkWhenRolIsFound() throws Exception {
        System.out.println("getRol");
        final long id = 1;

        mockMvc.perform(get(URI + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.descripcion").isString());
    }

    @Test
    public void testGetRol_isError404WhenRolIsNotFound() throws Exception {
        System.out.println("getRol");
        final long id = 2;
        mockMvc.perform(get(URI + "/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string(""));
    }
}
