package com.seguritech.practicafinal.repository;

import com.seguritech.practicafinal.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author wblanck
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
