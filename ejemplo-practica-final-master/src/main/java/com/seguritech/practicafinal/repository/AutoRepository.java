package com.seguritech.practicafinal.repository;

import com.seguritech.practicafinal.domain.Auto;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author wblanck
 */
public interface AutoRepository extends JpaRepository<Auto, Long> {

}
