package com.seguritech.practicafinal.domain;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Werner
 */
@Entity
public class Rol extends BaseEntity<Long> implements Serializable {

    private String descripcion;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
