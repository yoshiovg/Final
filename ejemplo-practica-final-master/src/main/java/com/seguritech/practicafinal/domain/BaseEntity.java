package com.seguritech.practicafinal.domain;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author wblanck
 * @param <ID> Type for the entity's ID
 */
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }
}
