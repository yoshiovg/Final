package com.seguritech.practicafinal.resource;

import com.seguritech.practicafinal.domain.Rol;
import com.seguritech.practicafinal.service.RolService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wblanck
 */
@RestController
@RequestMapping(RolResource.URI)
public class RolResource extends CrudResource<Long, Rol, RolService> {

    protected static final String URI = "/rol";

    public RolResource(RolService service) {
        super(service);
    }

    @Override
    protected String getBaseURI() {
        return URI;
    }
}
