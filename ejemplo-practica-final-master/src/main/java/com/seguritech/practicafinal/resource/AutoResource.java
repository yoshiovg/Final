package com.seguritech.practicafinal.resource;

import com.seguritech.practicafinal.domain.Auto;
import com.seguritech.practicafinal.service.AutoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wblanck
 */
@RestController
@RequestMapping(AutoResource.URI)
public class AutoResource extends CrudResource<Long, Auto, AutoService> {

    protected static final String URI = "/auto";

    public AutoResource(AutoService service) {
        super(service);
    }

    @Override
    protected String getBaseURI() {
        return URI;
    }
}
