package com.seguritech.practicafinal.resource;

import com.seguritech.practicafinal.domain.Usuario;
import com.seguritech.practicafinal.service.UsuarioService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wblanck
 */
@RestController
@RequestMapping(UsuarioResource.URI)
public class UsuarioResource extends CrudResource<Long, Usuario, UsuarioService> {

    protected static final String URI = "/usuario";

    public UsuarioResource(UsuarioService service) {
        super(service);
    }

    @Override
    protected String getBaseURI() {
        return URI;
    }

}
