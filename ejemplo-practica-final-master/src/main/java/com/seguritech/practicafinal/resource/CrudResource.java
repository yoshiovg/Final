package com.seguritech.practicafinal.resource;

import com.seguritech.practicafinal.domain.BaseEntity;
import com.seguritech.practicafinal.service.BaseService;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author Werner
 * @param <ID> Type of the Entity's ID
 * @param <E> Type of the Entity
 * @param <S> Service of the Entity
 */
// TODO improve exception handling with controller advices.
@CrossOrigin
public abstract class CrudResource<ID extends Serializable, E extends BaseEntity<ID>, S extends BaseService<ID, E>> {

    private final S service;

    public CrudResource(S repository) {
        this.service = repository;
    }

    protected abstract String getBaseURI();

    @GetMapping
    public List<E> getAll() {
        List<E> entities = service.findAll();
        return entities;
    }

    @GetMapping("/{id}")
    public ResponseEntity<E> get(@PathVariable("id") ID id) {
        E entity = service.findOne(id);
        if (entity == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(entity);
    }

    @PostMapping
    public ResponseEntity<E> create(@RequestBody E entity) throws URISyntaxException {
        if (entity.getId() != null) {
            return ResponseEntity.badRequest().header("X-error", "El id debe ser null").body(null);
        }
        E saved = service.saveAndFlush(entity);
        return ResponseEntity.created(new URI(getBaseURI() + "/" + saved.getId())).body(saved);
    }

    @PutMapping
    public ResponseEntity<E> update(@RequestBody E entity) throws Exception {
        if (entity.getId() == null) {
            return ResponseEntity.badRequest().header("X-error", "El id no debe ser null").body(null);
        }
        final boolean exists = service.findOne(entity.getId()) != null;
        if (!exists) {
            return ResponseEntity.notFound().header("X-error", "No existe el recurso con el Id especificado.").build();
        }
        E saved = service.saveAndFlush(entity);
        return ResponseEntity.ok().body(saved);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<E> delete(@PathVariable("id") ID id) {
        service.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
