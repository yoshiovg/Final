package com.seguritech.practicafinal.service;

import com.seguritech.practicafinal.domain.Auto;
import com.seguritech.practicafinal.repository.AutoRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author wblanck
 */
@Service
public class AutoService extends BaseService<Long, Auto>{
    
    public AutoService(AutoRepository repository) {
        super(repository);
    }
}
