package com.seguritech.practicafinal.service;

import com.seguritech.practicafinal.domain.Usuario;
import com.seguritech.practicafinal.repository.UsuarioRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author wblanck
 */
@Service
public class UsuarioService extends BaseService<Long, Usuario>{
    
    public UsuarioService(UsuarioRepository repository) {
        super(repository);
    }
}
