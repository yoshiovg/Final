package com.seguritech.practicafinal.service;

import com.seguritech.practicafinal.domain.BaseEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author wblanck
 * @param <ID>
 * @param <E>
 */
public abstract class BaseService<ID extends Serializable, E extends BaseEntity<ID>> {
    
    private final JpaRepository<E, ID> repository;

    public BaseService(JpaRepository<E, ID> repository) {
        this.repository = repository;
    }
    
    @Transactional(readOnly = true)
     public List<E> findAll() {
        return repository.findAll();
     };

     @Transactional
    public E saveAndFlush(E s) {
        return repository.saveAndFlush(s);
    };

    @Transactional(readOnly = true)
    public E findOne(ID id) {
        return repository.findOne(id);
    }
    
    @Transactional
    public void delete(ID id) {
        repository.delete(id);
    }
}
