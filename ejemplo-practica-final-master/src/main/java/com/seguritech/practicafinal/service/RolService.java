package com.seguritech.practicafinal.service;

import com.seguritech.practicafinal.domain.Rol;
import com.seguritech.practicafinal.repository.RolRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author wblanck
 */
@Service
public class RolService extends BaseService<Long, Rol> {

    public RolService(RolRepository repository) {
        super(repository);
    }
}
